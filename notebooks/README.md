# Examples in Notebooks

I run notebooks servers using Docker, Docker-Machine and VMWare Fusion on Mac OS X. The first think I need is a VM to run Docker with. I create that using: `docker-machine create --driver vmwarefusion default`. While running Docker I mount the notebooks directory on each VM as `/mnt/notebooks`.

## [Beaker](http://beakernotebook.com/)

Start beaker (I use Docker): 

```
docker-machine start default
eval (docker-machine env default) # For fish shell
docker run -v /Users/brian/workspace/auv-swagger/auv-rawdata-server/notebooks:/mnt/notebooks -p 8800:8800 -t beakernotebook/beaker
```

Find the IP to connect to with `docker-machine env default` which returns something like:

```
set -gx DOCKER_TLS_VERIFY "1";
set -gx DOCKER_HOST "tcp://172.16.199.144:2376";
set -gx DOCKER_CERT_PATH "/Users/brian/.docker/machine/machines/default";
set -gx DOCKER_MACHINE_NAME "default";
# Run this command to configure your shell:
# eval (docker-machine env default)
```

In your local browser connect to https://172.16.199.144:8800 with the password dumped to the console.

## [Spark Notebook](http://spark-notebook.io/)

Start spark-notebook using Docker:

```
docker-machine start default
eval (docker-machine env default) # For fish shell
docker run -v /Users/brian/workspace/auv-swagger/auv-rawdata-server/notebooks:/mnt/notebooks -p 9000:9000 andypetrella/spark-notebook:0.6.2-scala-2.11.7-spark-1.6.0-hadoop-2.7.2-with-hive-with-parquet
```

Find the IP to connect to with `docker-machine env default` which returns something like:

```
set -gx DOCKER_TLS_VERIFY "1";
set -gx DOCKER_HOST "tcp://172.16.199.144:2376";
set -gx DOCKER_CERT_PATH "/Users/brian/.docker/machine/machines/default";
set -gx DOCKER_MACHINE_NAME "default";
# Run this command to configure your shell:
# eval (docker-machine env default)
```

In your local browser connect to https://172.16.199.144:9000

## Other Notebooks

- [Apache Toree](https://toree.apache.org/), A [Jupyter](http://jupyter.org/) Kernel for [Scala/Spark](https://try.jupyter.org/)
- [IBM BlueMix](https://developer.ibm.com/clouddataservices/start-developing-with-spark-and-notebooks/)