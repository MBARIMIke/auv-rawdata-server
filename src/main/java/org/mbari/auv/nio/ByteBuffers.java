package org.mbari.auv.nio;

import java.lang.reflect.Method;
import java.nio.ByteBuffer;

/**
 * @author Brian Schlining
 * @since 2016-02-18T15:48:00
 */
public class ByteBuffers {

    /**
     * 'Unmaps' a MappedByteBuffer from memory. They don't seem to get GC'd correctly so we use reflection to
     * call `sun.misc.Clean`. See https://stackoverflow.com/questions/2972986/how-to-unmap-a-file-from-memory-mapped-using-filechannel-in-java
     * @param buf The ByteBuffer to garbage collect
     */
    public static void closeDirectBuffer(ByteBuffer buf) {
        if (buf.isDirect()) {
            // we could use this type cast and call functions without reflection code,
            // but static import from sun.* package is risky for non-SUN virtual machine.
            //try { ((sun.nio.ch.DirectBuffer)cb).cleaner().clean(); } catch (Exception ex)
            try {
                Method cleaner = buf.getClass().getMethod("cleaner");
                cleaner.setAccessible(true);
                Method clean = Class.forName("sun.misc.Cleaner").getMethod("clean");
                clean.setAccessible(true);
                clean.invoke(cleaner.invoke(buf));
            }
            catch (Exception e) {
                throw new RuntimeException("Failed to call 'clean' method on " + buf, e);
            }
        }
    }
}