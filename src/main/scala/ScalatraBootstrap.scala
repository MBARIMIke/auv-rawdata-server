import akka.actor.ActorSystem
import java.io.File
import java.nio.file.Paths
import javax.servlet.ServletContext

import org.mbari.auv.rawdata.api.DefaultV1Api
import org.mbari.auv.rawdata.controllers.InMemDataController
import org.mbari.auv.rawdata.db.CacheFile
import org.scalatra.LifeCycle
import com.typesafe.config.ConfigFactory
import org.mbari.auv.rawdata.model.{ CachedMissionDir, Mission }
import org.slf4j.LoggerFactory

class ScalatraBootstrap extends LifeCycle {

  private[this] val log = LoggerFactory.getLogger(getClass)

  implicit val swagger = new SwaggerApp

  override def init(context: ServletContext): Unit = {
    implicit val system = ActorSystem("appActorSystem")
    try {
      val config = ConfigFactory.load()

      // -- Read AUV directories from config file
      val auvDirectories = config.getString("auv.directories")
        .split(":")
        .map(Paths.get(_))

      // -- Read cache file location from config file
      val cacheFile = {
        val f = new File(config.getString("data.cache.file"))
        if (f.exists() && f.canRead) {
          log.info(s"Found data cache at ${f.getAbsolutePath}")
          Option(f)
        } else {
          log.info(s"No data cache at ${f.getAbsolutePath}")
          None
        }
      }

      // -- Import cache file if it exists
      val cachedDeployments = cacheFile.map(CacheFile(_))
        .getOrElse(Nil)
        .map(CachedMissionDir)
        .map(_.asInstanceOf[Mission])
        .toArray

      // -- Create our data controller.
      val dataController = new InMemDataController(auvDirectories, cachedDeployments)

      // -- Configure API
      val v1Api = new DefaultV1Api(dataController)

      context mount (v1Api, "/v1", "v1")
      context mount (new ResourcesApp, "/api-docs")
    } catch {
      case e: Throwable => e.printStackTrace()
    }
  }
}