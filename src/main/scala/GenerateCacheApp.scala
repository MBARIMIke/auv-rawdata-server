import java.nio.charset.Charset
import java.nio.file.{ Files, Paths }

import com.typesafe.config.ConfigFactory
import org.mbari.auv.rawdata.controllers.InMemDataController
import org.mbari.auv.rawdata.db.CacheFile

import scala.util.control.NonFatal

/**
 *
 *
 * @author Brian Schlining
 * @since 2016-03-07T16:19:00
 */
object GenerateCacheApp {

  def main(args: Array[String]): Unit = {
    try {
      val config = ConfigFactory.load()

      val auvDirectories = config.getString("auv.directories")
        .split(":")
        .map(Paths.get(_))

      val dataController = new InMemDataController(auvDirectories)
      val str = generateCacheData(dataController)

      val target = if (args.size > 0) Paths.get(args(0))
      else Paths.get(config.getString("data.cache.file"))

      val writer = Files.newBufferedWriter(target, Charset.forName("UTF-8"))
      writer.write(str, 0, str.size)
      writer.close()
    } catch {
      case NonFatal(e) => e.printStackTrace()
    }
  }

  def generateCacheData(dataController: InMemDataController): String = {

    // --- Load start and end dates for all missionDirectories
    val deployments = dataController.missions
    deployments.foreach(_.startDate) // This forces the navigation files to be read
    val info = deployments.map(_.asDeployment)
    CacheFile.deploymentsToJson(info)

  }

}
