package org.mbari.auv.rawdata.model

case class Error(
  code: Int,
  message: String,
  fields: String
)
