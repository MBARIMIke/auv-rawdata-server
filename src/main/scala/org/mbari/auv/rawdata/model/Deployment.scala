package org.mbari.auv.rawdata.model

import java.nio.file.Path
import java.time.Instant

case class Deployment(
    path: Path,
    vehicle: String,
    startDate: Instant,
    endDate: Instant
) {
  def name: String = path.getFileName.toString
  def asWebDeployment: WebDeployment = WebDeployment(name, vehicle, startDate, endDate)
}

case class WebDeployment(name: String, vehicle: String, startDate: Instant, endDate: Instant)