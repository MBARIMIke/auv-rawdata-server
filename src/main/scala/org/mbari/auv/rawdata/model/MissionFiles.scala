package org.mbari.auv.rawdata.model

/**
 * Case class the represents a deployment. This includes 2 pieces of information:
 * The name of the deployment and the names of the files in the deployments.
 * This class is mostly included to represent data that's serialized out to JSON.
 *
 * @author Brian Schlining
 * @since 2016-02-26T13:23:00
 */
case class MissionFiles(deployment: String, names: Seq[String])
