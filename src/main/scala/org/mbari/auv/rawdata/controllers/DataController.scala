package org.mbari.auv.rawdata.controllers

import java.time.Instant
import java.nio.file.Path

import org.mbari.auv.rawdata.model.Mission
import org.mbari.auv.io.{ LogRecord, LogRecordDescription }

trait DataController {

  def deployment(name: String): Option[Mission]

  def missions: Set[Mission]

  def missions(from: Option[Instant], to: Option[Instant]): Set[Mission]

  def files(deploymentName: String): Seq[Path]

  def data(log: Path): Seq[LogRecord[Double]]

  def header(log: Path): Seq[LogRecordDescription]

}
