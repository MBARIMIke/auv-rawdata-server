package org.mbari.auv.rawdata.db

import java.io.{ BufferedWriter, File, FileWriter }
import java.net.URL
import java.nio.charset.Charset
import java.nio.file.{ Files, OpenOption, Path, Paths }
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.{ Date, TimeZone }

import org.mbari.auv.rawdata.model.Deployment
import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization
import org.json4s.native.Serialization.writePretty
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.control.NonFatal

/**
 *
 *
 * @author Brian Schlining
 * @since 2016-03-08T10:33:00
 */
object CacheFile {

  private[this] val log = LoggerFactory.getLogger(getClass)

  // JSON4S date serializer was broken and would not do a round trip
  // override it and use our own formater
  //  private val dateFormat = {
  //    val df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
  //    df.setTimeZone(TimeZone.getTimeZone("UTC"))
  //    df
  //  }
  //
  //  private[this] val customDateFormat = new CustomSerializer[Date](format => (
  //    { case JString(s) => dateFormat.parse(s) },
  //    { case d: Date => JString(dateFormat.format(d)) }
  //  ))

  private[this] val customInstantFormat = new CustomSerializer[Instant](format => (
    { case JString(s) => Instant.parse(s) },
    { case i: Instant => JString(i.toString) }
  ))

  private[this] val pathFormat = new CustomSerializer[Path](format => (
    { case JString(s) => Paths.get(s) },
    { case p: Path => JString(p.toAbsolutePath.toString) }
  ))

  def apply(cacheFile: File): Iterable[Deployment] = {
    val source = Source.fromFile(cacheFile)
    val text = source.mkString
    val deployments = jsonToDeployments(text)
    source.close()
    deployments
  }

  def apply(cacheURL: URL): Iterable[Deployment] = {
    val source = Source.fromURL(cacheURL)
    val text = source.mkString
    val deployments = jsonToDeployments(text)
    source.close()
    deployments
  }

  def jsonToDeployments(s: String): Iterable[Deployment] = {
    implicit val formats = DefaultFormats + customInstantFormat + pathFormat
    parse(s).extract[Array[Deployment]]
  }

  def deploymentsToJson(deployments: Iterable[Deployment]): String = {
    implicit val formats = Serialization.formats(NoTypeHints) + customInstantFormat + pathFormat
    val sortedDeployment = deployments.toArray.sortBy(d => d.path.toAbsolutePath.toString)
    writePretty(sortedDeployment)
  }

  def writeAsync(deployments: Iterable[Deployment], target: Path): Unit = {
    val thread = new Thread(() => {
      log.info("Writing deployment cache to {}", target.toAbsolutePath)
      try {
        val cacheData = CacheFile.deploymentsToJson(deployments)
        val writer = Files.newBufferedWriter(target, Charset.forName("UTF-8"))
        writer.write(cacheData)
        writer.close()
      } catch {
        case NonFatal(e) => log.warn(s"Failed to write ${target.toAbsolutePath}", e)
      }
    }, "CacheFile-writeAsync")
    thread.setDaemon(true)
    thread.start()
  }

}
