package org.mbari.auv.rawdata.db

import java.nio.file._

import org.mbari.auv.rawdata.model.{ Mission, MissionDir }
import org.slf4j.LoggerFactory

import scala.io.Source

/**
 * Parse catalog files. These files should be located at the root of any directory listed
 * in the config file at `auv.directories`. They contains a listing of all the valid AUV missions
 * in that directory. If a catalog files is found, then that directory will not be walked. Only
 * the missions in the files will be used.
 * @author Brian Schlining
 * @since 2017-03-29T08:31:00
 */
object CatalogFile {

  private[this] val log = LoggerFactory.getLogger(getClass)

  def apply(
    path: Path
  ): Iterable[Mission] = {
    log.debug("Loading catalog from {}", path.toAbsolutePath.toString)
    val source = Source.fromFile(path.toFile)
    val lines = source.getLines()
      .map(_.trim)
      .filter(_.nonEmpty)
      .filter(!_.startsWith("#"))
    val parent = Option(path.getParent).getOrElse(Paths.get("/"))
    val missions = lines.map(l => parent.resolve(l))
      .filter(p => Files.exists(p))
      .map(p => new MissionDir(p))
      .toSet
    source.close()
    missions
  }
}
