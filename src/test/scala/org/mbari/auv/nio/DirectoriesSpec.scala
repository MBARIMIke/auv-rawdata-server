package org.mbari.auv.nio

import java.io.File

import org.scalatest.{ FlatSpec, Matchers }

/**
 * @author Brian Schlining
 * @since 2017-03-27T16:58:00
 */
class DirectoriesSpec extends FlatSpec with Matchers {

  private[this] val path = {
    val url = getClass.getResource("/data/2010322/2010.322.03")
    new File(url.toURI).toPath
  }

  "Directories" should "list all files" in {
    val xs = Directories.list(path)
    xs.size should be(4)
  }

  it should "list files filtered using a predicate function" in {
    val xs = Directories.list(path, (p) => p.endsWith("gps.log"))
    xs.size() should be(1)
  }

  it should "list files filtered using a glob expression" in {
    val xs = Directories.list(path, "gps*")
    xs.size() should be(1)

    val ys = Directories.list(path, "*.log")
    ys.size() should be(4)
  }

}
