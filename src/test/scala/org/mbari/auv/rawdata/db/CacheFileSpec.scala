package org.mbari.auv.rawdata.db

import java.nio.file.Paths
import java.time.Instant
import java.util.Date

import org.mbari.auv.rawdata.model.Deployment
import org.mbari.net.URLUtilities
import org.scalatest.{ FlatSpec, Matchers }

import scala.io.Source

/**
 *
 *
 * @author Brian Schlining
 * @since 2016-03-08T11:30:00
 */
class CacheFileSpec extends FlatSpec with Matchers {

  "A CacheFile" should "parse JSON to Deployments" in {
    val json =
      """
        |[{
        |    "path":"/mnt/AUVBI/2010.074.04",
        |    "vehicle":"Dorado123",
        |    "startDate":"2010-03-15T12:27:08Z",
        |    "endDate":"2010-03-15T12:27:50Z"
        |  },
        |  {
        |    "path":"/Volumes/AUVCTD/missionlogs/2010.027.10",
        |    "vehicle":"Multibeam",
        |    "startDate":"2010-01-27T20:44:06Z",
        |    "endDate":"2010-01-27T21:50:58Z"
        |  }]
      """.stripMargin
    val deployments = CacheFile.jsonToDeployments(json)
    deployments.size should be(2)
    deployments.head.name should be("2010.074.04")
    val startDate = Instant.parse("2010-03-15T12:27:08Z")
    deployments.head.startDate should be(startDate)
  }

  it should "parse JSON in a file to Deployments" in {
    val url = getClass.getResource("/backup-cache.json")
    val deployments = CacheFile(url)
    deployments.size should be(101)
  }

  it should "convert deployments to JSON" in {
    val url = getClass.getResource("/backup-cache.json")
    val deployments = CacheFile(url)
    val json = CacheFile.deploymentsToJson(deployments)
    val deployments2 = CacheFile.jsonToDeployments(json)
    deployments.size should be(deployments2.size)
    //print(json)
  }

  it should "format dates correctly" in {
    val dateString = "2010-03-15T12:27:08Z"
    val startDate = Instant.parse(dateString)
    val deployment = Deployment(Paths.get("foo"), "Dorado389", startDate, startDate)
    val json = CacheFile.deploymentsToJson(Seq(deployment))
    //println(json)
    val deployment2 = CacheFile.jsonToDeployments(json)
    deployment2.size should be(1)
    deployment.startDate should be(deployment2.head.startDate)
    //println(deployment2)
  }

}
