package org.mbari.auv.rawdata.db

import java.nio.file.Paths

import org.scalatest.{ FlatSpec, Matchers }

/**
 * @author Brian Schlining
 * @since 2017-03-29T10:23:00
 */
class CatalogFileSpec extends FlatSpec with Matchers {

  private[this] val path = Paths.get(getClass.getResource("/AUV_MISSION_CATALOG.txt").toURI)

  "CatalogFile" should "parse" in {
    val missions = CatalogFile(path)
    //println(missions.toList.head.absolutePath)
    missions.size should be(1)
  }

}
