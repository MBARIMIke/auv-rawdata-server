package org.mbari.auv.rawdata.model

import java.nio.file.Paths

import org.mbari.auv.rawdata.db.CacheFile
import org.scalatest.{FlatSpec, Matchers}

/**
  * @author Brian Schlining
  * @since 2017-03-29T15:09:00
  */
class MissionSpec extends FlatSpec with Matchers {

  val paths = List(
    Paths.get(getClass.getResource("/data/2010322/2010.322.03").toURI),
    Paths.get(getClass.getResource("/data/2010040/2010.040.02").toURI),
    Paths.get(getClass.getResource("/data/2017017/2017.017.00").toURI))


  "MissionDir" should "construct" in {
    val m = new MissionDir(paths(0))
    m.startDate should not be empty
    m.endDate should not be empty
    m.name should be ("2010.322.03")
    m.vehicle should be ("Dorado389")
  }

  it should "transform via asDeployment" in {
    val m = new MissionDir(paths(0))
    val d = m.asDeployment
    m.startDate.get should be (d.startDate)
    m.endDate.get should be (d.endDate)
    m.path should be (d.path)
    m.vehicle should be(d.vehicle)
  }


  "Mission" should "binarySearch" in {
    val ms: Array[Mission] = paths.map(new MissionDir(_))
      .sortBy(_.absolutePath)
      .toArray
    val idx0 = Mission.binarySearch(ms, ms(0).absolutePath)
    idx0 should be (0)
    val idx1 = Mission.binarySearch(ms, ms(1).absolutePath)
    idx1 should be (1)
    val idx2 = Mission.binarySearch(ms, "foo")
    idx2 should be < 0
  }

  it should "binarySearch many Deployments" in {
    val cachePath = Paths.get(getClass.getResource("/mission-cache.json").toURI)
    val missions: Array[Mission] = CacheFile(cachePath.toFile).map(CachedMissionDir)
      .toArray
      .map(_.asInstanceOf[Mission])
      .sortBy(_.absolutePath)
    val idx0 = Mission.binarySearch(missions, "/Volumes/AUVBI/missionlogs/2009/2009181/2009.181.00")
    idx0 should be >= 0
  }

  "CachedMissionDir" should "construct" in {
    val m = new MissionDir(paths(0))
    val c = CachedMissionDir(m.asDeployment)
    m.path should be (c.path)
    m.absolutePath should be (c.absolutePath)
    m.startDate.get should be (c.startDate.get)
    m.endDate.get should be (c.endDate.get)
    m.name should be (c.name)
    m.vehicle should be (c.vehicle)
  }


}
