val akkaVersion = "2.5.22"
val auvioVersion = "1.0.2"
val codecVersion = "1.12"
val configVersion = "1.3.4"
val jettyVersion = "9.4.18.v20190429"
val json4sVersion = "3.6.5"
val scalatestVersion = "3.0.7"
val scalatraVersion = "2.6.5"
val scilubeVersion = "2.0.4"
val servletVersion = "3.1.0"


lazy val buildSettings = Seq(
  organization := "org.mbari.auv",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.12.8",
  crossScalaVersions := Seq("2.12.8")
)

lazy val consoleSettings = Seq(
  shellPrompt := { state =>
    val user = System.getProperty("user.name")
    user + "@" + Project.extract(state).currentRef.project + ":sbt> "
  },
  initialCommands in console :=
    """
      |import java.time.Instant
      |import java.util.UUID
    """.stripMargin
)

lazy val dependencySettings = Seq(
  resolvers ++= Seq(Resolver.mavenLocal,
    "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
    "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/",
    "hohonuuli-bintray" at "http://dl.bintray.com/hohonuuli/maven",
    "unidata repo" at "https://artifacts.unidata.ucar.edu/content/repositories/unidata-releases/",
    "geotoolkit repo" at "http://maven.geotoolkit.org"
  )
)

lazy val optionSettings = Seq(
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding", "UTF-8", // yes, this is 2 args
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-unchecked",
    "-Xlint",
    "-Yno-adapted-args",
    "-Xfuture"),
  javacOptions ++= Seq("-target", "1.8", "-source", "1.8"),
  incOptions := incOptions.value.withNameHashing(true),
  updateOptions := updateOptions.value.withCachedResolution(true)
)

lazy val nativePackagerSettings = Seq(
  // -- Native packager
  maintainer in Linux := "Brian Schlining <brian@mbari.org>",
  packageSummary in Linux := "AUV Log file server",
  packageDescription := "AUV log discovery and parsing via REST"
)

lazy val appSettings = buildSettings ++
  consoleSettings ++
  dependencySettings ++
  optionSettings ++
  nativePackagerSettings ++
  Seq(
    todosTags := Set("TODO", "FIXME", "WTF"),
    fork := true,
    ivyXML := <dependencies>
      <exclude module="slf4j-log4j12"/>
      <exclude module="grizzled-slf4j_2.9.1"/>
      <exclude module="jsr311-api" />
    </dependencies>
  )

val apps = Seq("jetty-main", "generate-cache-app")  // for sbt-pack

lazy val `auv-rawdata-server` = (project in file("."))
    .enablePlugins(JettyPlugin, JavaServerAppPackaging)
  .settings(appSettings)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe"             % "config"                         % configVersion,
      "com.typesafe.akka"       %% "akka-actor"                     % akkaVersion,
      "commons-codec"            % "commons-codec"                  % codecVersion,
      "javax.servlet"            % "javax.servlet-api"              % servletVersion               ,
      "org.eclipse.jetty"        % "jetty-server"                   % jettyVersion          % "container;compile;test",
      "org.eclipse.jetty"        % "jetty-servlets"                 % jettyVersion          % "container;compile;test",
      "org.eclipse.jetty"        % "jetty-webapp"                   % jettyVersion          % "container;compile;test",
      "org.json4s"              %% "json4s-native"                 % json4sVersion,
      "org.mbari.auv"           %% "auv-log-io"                     % auvioVersion,
      "org.scalatest"           %% "scalatest"                      % scalatestVersion               % "test",
      "org.scalatra"            %% "scalatra"                       % scalatraVersion,
      "org.scalatra"            %% "scalatra-json"                  % scalatraVersion,
      "org.scalatra"            %% "scalatra-scalate"               % scalatraVersion,
      "org.scalatra"            %% "scalatra-swagger"               % scalatraVersion
      //"scilube"                 %% "scilube-core"                   % scilubeVersion
      //"scilube"                 %% "scilube-extensions"             % scilubeVersion
      //"scilube"                 %% "scilube-gis"                    % scilubeVersion
    ).map(_.excludeAll(ExclusionRule("org.slf4j", "slf4j-jdk14"),
      ExclusionRule("javax.servlet", "servlet-api"))),
    mainClass in assembly := Some("JettyMain")
  )
  .settings(
    packAutoSettings ++ Seq(packExtraClasspath := apps.map(_ -> Seq("${PROG_HOME}/conf")).toMap,
      packJvmOpts := apps.map(_ -> Seq("-Duser.timezone=UTC", "-Xmx4g", "-Djdk.nio.maxCachedBufferSize=262144")).toMap,
      packDuplicateJarStrategy := "latest",
      packJarNameConvention := "original")
  )
// https://www.evanjones.ca/java-bytebuffer-leak.html




// -- SCALARIFORM
// Format code on save with scalariform

import scalariform.formatter.preferences._
import com.typesafe.sbt.SbtScalariform

SbtScalariform.scalariformSettings

SbtScalariform.ScalariformKeys.preferences := SbtScalariform.ScalariformKeys.preferences.value
  .setPreference(IndentSpaces, 2)
  .setPreference(PlaceScaladocAsterisksBeneathSecondAsterisk, false)
  .setPreference(DoubleIndentClassDeclaration, true)


// -- Aliases ---------------------------------------------------------
addCommandAlias("cleanall", ";clean;clean-files")
